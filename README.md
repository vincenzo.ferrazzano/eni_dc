Welcome to Vincenzo Ferrazzano's solution for the ENI data challenge!

# Installation
 - open a window command line (cmd) and cd to the repo folder 
 - Create a virtual environment (say, `venv`) and activate it:
 ```
 python -m venv .\venv
 .\venv\scripts\activate
 ```
Make sure that you have python 3.7 installed and your `python` command points to that interpreter 
- Move to the `code` sub-folder and install the package with its dependencies (might take some time)
 ```
cd code
pip install .
 ```

# Use the package
Just open presentation/vf_data_challenge_notebook.ipynb jupyter notebook and run it from top. 
Might take a while to execute 

# Presentation
The PDF slides are located in presentation/VF_slides.pdf