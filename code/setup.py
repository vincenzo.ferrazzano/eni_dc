import os

import setuptools


class PkgSpecs:
    name = "vf_eni_dc"
    MAJOR = os.getenv('MAJOR', 0)
    MINOR = os.getenv('MINOR', 0)
    PATCH = os.getenv('PATCH', 0)
    author = "Vincenzo Ferrazzano"
    author_email = "vincenzo.ferrazzano@gmail.com"
    short_description = "Package for the ENI Data Challenge"
    python_requires = ">=3.7"
    OS = "OS Independent"
    url = "https://gitlab.com/vincenzo.ferrazzano/eni_dc"
    deps = ['pandas==1.3.5',
            'tqdm==4.62.3',
            'scipy==1.7.3',
            'statsmodels==0.13.2',
            'matplotlib==3.5.3',
            'scikit-learn==1.0.2',
            'seaborn==0.12.1',
            'xlrd==2.0.1',
            'jupyter==1.0.0'
            ]


version_str = f"{PkgSpecs.MAJOR}.{PkgSpecs.MINOR}.{PkgSpecs.PATCH}"

with open("../README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name=PkgSpecs.name,
    version=version_str,
    author=PkgSpecs.author,
    author_email=PkgSpecs.author_email,
    description=PkgSpecs.short_description,
    long_description=long_description,
    long_description_content_type="text/markdown",
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        f"Operating System :: {PkgSpecs.OS}",
    ],
    url=PkgSpecs.url,
    install_requires=PkgSpecs.deps,
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=PkgSpecs.python_requires
)
