from __future__ import annotations

import math
from typing import Sequence, Dict

import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.model_selection import StratifiedShuffleSplit
from scipy.stats import spearmanr
from matplotlib import pyplot as plt


class DataClass:
    _feature_cols = ['LBE', 'LB', 'AC', 'FM', 'UC', 'ASTV', 'MSTV', 'ALTV', 'MLTV', 'DL',
                     # 'DS',
                     'DP',
                     # 'DR', # Removed as it is constant, as reported on the Description
                     'Width', 'Min', 'Max', 'Nmax', 'Nzeros', 'Mode', 'Mean', 'Median', 'Variance',
                     'Tendency',
                     'b', 'e']
    _cols_to_encode = ['Tendency']
    _out_col = 'NSP'

    def __init__(self, x_df: pd.DataFrame,
                 y_series: pd.Series,
                 encode=True,
                 rescale=True,
                 log_tran: Sequence[str] = None,
                 permute_cols=False,
                 normalize_by: Dict[Sequence[str], str] = None):
        assert x_df.index.equals(y_series.index)
        self._raw_x = x_df.copy()
        self._raw_y = y_series.copy()

        encoded_x = None
        if encode:
            encoded_x = pd.concat(
                (pd.get_dummies(x_df.pop(c), prefix=c, drop_first=True) for c in DataClass._cols_to_encode),
                axis=1)
        duration = DataClass.compute_delta_time(x_df)
        post_processed_x = pd.concat((x_df, duration), axis=1)
        if log_tran is not None:
            post_processed_x[log_tran] = np.log(1 + post_processed_x[log_tran])
        if rescale:
            min_max_scaler = preprocessing.StandardScaler()
            post_processed_x = pd.DataFrame(data=min_max_scaler.fit_transform(post_processed_x),
                                            index=post_processed_x.index,
                                            columns=post_processed_x.columns)
        if encoded_x is not None:
            post_processed_x = pd.concat((post_processed_x, encoded_x), axis=1)
        if normalize_by is not None:
            for keys, v in normalize_by.items():
                normalization = post_processed_x[v]
                for k in keys:
                    src = post_processed_x.pop(k)
                    norm = src / normalization
                    post_processed_x[f'{k}_frac_{v}'] = norm
        if permute_cols:
            gen = np.random.default_rng(1789)
            post_processed_x = post_processed_x[gen.permutation(post_processed_x.columns)]
        # we drop missing y and the corresponding x
        entries_to_drop_x_ind, entries_to_drop_y_ind = self._get_missing_data_ind()
        entries_to_keep_x = entries_to_drop_x_ind[~entries_to_drop_x_ind].index
        entries_to_keep_y = entries_to_drop_y_ind[~entries_to_drop_y_ind].index
        entries_to_keep = entries_to_keep_y.intersection(entries_to_keep_x)
        self._x = post_processed_x.loc[entries_to_keep]
        self._y = self._raw_y.loc[entries_to_keep]
        y_rounded = self._y.astype(int)
        # cast them if possible
        if (y_rounded - self._y).abs().max() == 0:
            self._y = y_rounded

    def get_output_frequency(self):
        return self.compute_freq(self._y)

    @staticmethod
    def compute_delta_time(df):
        res = df.pop('e') - df.pop('b')
        res.name = 'time_duration'
        return res

    @staticmethod
    def compute_freq(outcomes: pd.Series):
        frequency = {}
        for val in set(outcomes):
            frequency[val] = (outcomes == val).sum()
        frequency = pd.Series(frequency)
        return frequency

    def scatterplot_vs_duration(self, cols=None):
        df = self._x.set_index(self._x['time_duration'])
        if cols is not None:
            df = df[cols]
        fig = plt.figure(1)
        n_plots = len(df.columns)
        n_cols = int(math.sqrt(n_plots)) + 1
        n_rows = n_plots // n_cols
        if n_plots % n_cols != 0:
            n_rows += 1

        pos = range(n_plots)
        for c, k in zip(df, pos):
            ax = fig.add_subplot(n_rows, n_cols, pos[k] + 1)
            ax.scatter(df.index.values, df[c].values)
            ax.set_title(c)
        plt.xlabel("index")
        plt.ylabel("value")
        plt.show()

    def _get_missing_data_ind(self):
        entries_to_drop_y_ind = self._raw_y.isnull()
        entries_to_drop_x_ind = self._raw_x.isnull().sum(axis=1) > 0
        return entries_to_drop_x_ind, entries_to_drop_y_ind

    def data_report(self):
        missing_x, missing_y = self._get_missing_data_ind()

        def spam_missing(missing, label):
            n_missing = missing.sum()
            if n_missing > 0:
                print(f'{label} has #{n_missing} missing data: {missing[missing].index} out of {len(missing)}')

        spam_missing(missing_x, 'X')
        spam_missing(missing_y, 'y')
        self._recap_y_stats(self._y, 'Whole Dataset')
        self._x.hist(figsize=(16, 12), bins=int(math.sqrt(len(self._x))))
        rank_corr = spearmanr(self._x)
        plt.matshow(rank_corr.correlation, cmap='gray')
        plt.title('Spearman correlation X values')
        plt.show()

    @staticmethod
    def from_xlsx(xlsx_path: str,
                  encode: bool,
                  rescale: bool,
                  sheet='Raw Data',
                  log_tran: Sequence[str] = None,
                  permute_cols=False,
                  normalize_by: Dict[Sequence[str], str] = None,
                  drop_last=2):
        df = pd.read_excel(xlsx_path, sheet_name=sheet).dropna(how='all')[:-drop_last]
        x = df[DataClass._feature_cols]
        y = df[DataClass._out_col]
        return DataClass(x, y, encode=encode,
                         rescale=rescale, log_tran=log_tran,
                         permute_cols=permute_cols,
                         normalize_by=normalize_by)

    def stratified_train_test_split(self, test_size=0.2, random_state=42, verbose=True):
        a = StratifiedShuffleSplit(n_splits=1, test_size=test_size, random_state=random_state)
        train, test = next(a.split(self.x, self.y))
        y_train = self._y.iloc[train]
        y_test = self._y.iloc[test]
        if verbose:
            self._recap_y_stats(y_train, 'Train')
            self._recap_y_stats(y_test, 'Test')
        return (self._x.iloc[train], y_train), (self._x.iloc[test], y_test)

    def _recap_y_stats(self, y: pd.Series, name: str):
        s = self.compute_freq(y)
        s.index.name = 'outcome'
        stats = pd.concat({'prob': s / len(y), 'counts': s}, axis=1)
        print(
            f'{name} Outcomes: \n{stats}\nout of {len(y)} samples'
        )

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y


if __name__ == '__main__':
    data = DataClass.from_xlsx('../../../data/cardiotocography/CTG.xls', encode=False, rescale=False,
                               drop_last=2,
                               )
    data.scatterplot_vs_duration(['AC', 'UC', 'DL', 'DS', 'DP', 'Nmax', 'Nzeros'])
