from typing import Sequence, Dict

import numpy as np
import pandas as pd
import pylab as pl
from matplotlib import pyplot as plt

from sklearn.metrics import classification_report, confusion_matrix, make_scorer, ConfusionMatrixDisplay
from sklearn.model_selection import cross_val_predict, RandomizedSearchCV
from sklearn.metrics import f1_score

from vf_eni_dc.data import DataClass


class PerformanceMeasuring:
    def __init__(self, data_class: DataClass, n_performance_fold: int, test_size: float):
        self.n_performance_fold = n_performance_fold
        self.size_test = test_size
        self.cv_n_jobs = 4
        self.train_subset, self.test_subset = data_class.stratified_train_test_split(self.size_test, verbose=False)
        self.fitting_result = {}

    def compute_confusion_matrix_from_train_set(self, estimator, normalize=False, **kwargs):
        x_train, y_train = self.train_subset
        y_train_predict = cross_val_predict(estimator,
                                            x_train,
                                            y_train,
                                            cv=self.n_performance_fold,
                                            n_jobs=self.cv_n_jobs,
                                            **kwargs)
        matrix = confusion_matrix(y_train, y_train_predict)
        freq = DataClass.compute_freq(y_train)
        confusion_df = pd.DataFrame(data=matrix,
                                    index=freq.index.values,
                                    columns=freq.index.values)
        print(classification_report(y_train, y_train_predict))
        assert confusion_df.sum(axis=1).equals(freq)
        if normalize:
            return confusion_df.div(freq, axis=0)
        return confusion_df

    @staticmethod
    def compute_confusion_metric(confusion: pd.DataFrame, weights: pd.DataFrame = None):
        delta_confusion = (confusion - np.eye(*confusion.shape)).abs()
        if weights is None:
            weights = 0.5
        delta_metric = (delta_confusion * weights).sum().sum()
        return delta_metric

    @staticmethod
    def compute_stats_from_confusion(confusion_mat: pd.DataFrame):
        precision_dict = {}
        recall_dict = {}

        for label, col in confusion_mat.items():
            row = confusion_mat.loc[label]
            tp = col[label]
            pred_sum = col.sum()
            true_sum = row.sum()
            precision = tp / pred_sum
            recall = tp / true_sum
            precision_dict[label] = precision
            recall_dict[label] = recall

        precision = pd.Series(precision_dict)
        recall = pd.Series(recall_dict)

        f_score = 2 * precision * recall / (precision + recall)
        return precision, recall, f_score

    def run_random_search(self,
                          estimator,
                          grid_parameter: Dict[str, Sequence],
                          n_cv_grid_search: int,
                          n_samples_grid=100,
                          random_state_grid=25):
        x_train, y_train = self.train_subset

        custom_f1_scorer = make_scorer(
            f1_score, greater_is_better=True, average="macro"
        )

        cv = RandomizedSearchCV(estimator,
                                grid_parameter,
                                scoring=custom_f1_scorer,
                                n_iter=n_samples_grid,
                                cv=n_cv_grid_search,
                                verbose=2,
                                random_state=random_state_grid,
                                n_jobs=-1
                                )
        res = cv.fit(x_train, y_train)
        print(f'{res.best_estimator_}')
        print(f'{res.best_score_}')
        print(f'{res.best_params_}')
        return res

    def performance_on_test_set(self, search_result: RandomizedSearchCV):
        x_train, y_train = self.train_subset
        # fit on train
        best_cls = search_result.best_estimator_.fit(x_train, y_train)
        x_test, y_test = self.test_subset
        name = type(search_result.best_estimator_).__name__
        # predict on test
        test_y_pred = best_cls.predict(x_test)
        self._spam_stats(name, test_y_pred, y_test)
        return best_cls

    def fit_and_store(self, classifier_obj):
        model_name = type(classifier_obj).__name__
        print(f'*** {model_name} ***')
        confusion_mat = self.compute_confusion_matrix_from_train_set(classifier_obj)
        PerformanceMeasuring._spam_stats_from_matrix(confusion_mat.values, model_name)
        self.fitting_result[model_name] = classifier_obj, confusion_mat

    def get_quick_fitting_results(self):
        f1 = pd.Series({k: self.compute_stats_from_confusion(v[1])[2].mean() for k, v in self.fitting_result.items()})
        return f1.sort_values(ascending=False)

    @staticmethod
    def _spam_stats(name: str, predicted_y: np.ndarray, actual_y: np.ndarray):
        print(classification_report(actual_y, predicted_y))
        matrix = confusion_matrix(actual_y, predicted_y)
        PerformanceMeasuring._spam_stats_from_matrix(matrix, name)

    @staticmethod
    def _spam_stats_from_matrix(matrix: np.ndarray, name: str):
        fig, ax = plt.subplots(figsize=(8, 5))
        cmp = ConfusionMatrixDisplay(
            matrix,
        )
        cmp.plot(ax=ax)
        pl.title(f'Confusion matrix: {name}')


if __name__ == '__main__':
    from sklearn.ensemble import VotingClassifier, GradientBoostingClassifier
    from sklearn.linear_model import LogisticRegression
    from sklearn.naive_bayes import GaussianNB
    cv_fold = 5
    test_size = 0.2
    data = DataClass.from_xlsx(fr'C:\Users\Enzo\ENI\data\cardiotocography\CTG.xls', encode=True, rescale=True,
                               log_tran=['AC', 'FM', 'UC', 'MSTV', 'ALTV', 'DP', 'Nzeros', 'Nmax', 'Variance', 'DL',
                                         'MLTV'],
                               permute_cols=True,
                               drop_last=2)
    data.data_report()
    performance = PerformanceMeasuring(data_class=data, n_performance_fold=cv_fold, test_size=test_size)
    for m in (LogisticRegression(max_iter=10000), GaussianNB(), GradientBoostingClassifier(random_state=24)):
        performance.fit_and_store(m)
    voters = (LogisticRegression(max_iter=10000), GaussianNB(), GradientBoostingClassifier(random_state=24))
    voting = VotingClassifier([(type(k).__name__, v[0]) for v in voters])
    performance.fit_and_store(voting)
